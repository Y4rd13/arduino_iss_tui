import time, _thread, serial

global flag, puerto
flag=1

def data(par,archivo):
	while (1):
		if(flag==1):
			dato = par.readline().decode("utf-8").rstrip('\n').rstrip('\r')
			archivo.write(dato)
			archivo.write('\n')


def main():
	global puerto, system
	pre=time.strftime('%y%m%d')+'_'+time.strftime("%H%M%S")
	f = open(pre+'_data.csv', 'w')
	arduino = serial.Serial(puerto, 9600)
	print('Iniciando...\n')
	print('Conectandose al '+puerto+'\n')
	time.sleep(2)
	if(arduino.isOpen()):
		print('Recibiendo datos...\n')
		_thread.start_new_thread(data, (arduino,f,))
		x=input("*Presione tecla ENTER para finalizar y guardar\n")
		global flag
		flag=2
		print('Guardando...\n')
		time.sleep(2)
		f.close()
		arduino.close();
		print('ARCHIVO GUARDADO')
	else:
		print('El Puerto está cerrado')


def ini():
	global puerto, system
	found = False

	print('=======================================\n')
	print('               DATA READ v2.0\n')
	print('=======================================\n')

	for iPuerto in range(0, 20):
	    try:
	        puerto = 'COM' + str(iPuerto)
	        Arduino = serial.Serial(puerto,9600)
	        Arduino.close()
	        found = True
	        break
	    except:
	        # Si hay error, no hacemos nada y continuamos con la busqueda
	        pass

	# Si encontramos el puerto?
	if found:
	    main()
	else:
	    print('La placa Arduino no está conectada')
	    input('Presione la tecla ENTER para salir...')
	    
ini()





